import { Dummycomponent } from './dummy.component';
import { DASHBOARD_ROUTES } from './dashboard/dashboard.routing';
import { AuthGuard } from './authentication/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SigninComponent } from './authentication/signin/signin.component';
import { Routes, RouterModule } from "@angular/router";

const APP_ROUTES: Routes = [

    { path: '', redirectTo:'/signin', pathMatch: 'full' },
    { path: 'signin', component: SigninComponent },
     { path: 'dummy', component: Dummycomponent },
    { path: 'dashboard', component: DashboardComponent, children: DASHBOARD_ROUTES, canActivateChild: [AuthGuard] },
    { path: '**', redirectTo:'/signin' }
];
export const routing = RouterModule.forRoot(APP_ROUTES);