import { PayComponent } from './pay/pay.component';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { ImageUploadModule } from 'ng2-imageupload';
//import { DashboardRouting } from './dashboard.routing';
import { BulkOrderComponent } from './bulk_order/bulk_order.component';
import { OrderHistoryComponent } from './order-history/order_history.component';
import { ShipPackageComponent } from './Ship_Package/ship_package.component';

import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HttpModule} from "@angular/http";
import { FormsModule } from "@angular/forms";

@NgModule({
    declarations: [
        ShipPackageComponent,
        BulkOrderComponent,
        OrderHistoryComponent,
        PayComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ImageUploadModule,
        AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCHAAF-2jzmx-G06WCapwMZK0VG3pIC3yY'
    })
    ],
})
export class DashboardModule {

}