import { NgForm } from '@angular/forms';
import { OrderModel } from './../../front_end_models/orderModel';
import { ImageResult, ResizeOptions, ImageUploadModule } from 'ng2-imageupload';
import { ErrorService } from './../../errors/error.service';
import { ApiMessageService } from './../../authentication/apimessages.service';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { Component, OnInit, NgZone, Attribute } from '@angular/core';


@Component({
    selector: 'order_history',
    templateUrl: './bulk_order.component.html',
    styleUrls: ['./bulk_order.component.css']
})
export class BulkOrderComponent {
imageDel = "./images/del_icon.png";
   imageSearch = "./images/search.png";
    imageBag = "./images/Bag.png";
    imagemap = "./images/map.png";
    imageusericon = "./images/usericon.png";
    imageupload = "./images/upload.png";
    imagecall = "./images/call.png";
    map_popup:boolean = false;
    UserName;
    url:string = 'http://35.164.84.176';
    title: string = 'My first angular2-google-maps project';
    lat: number;
    lng: number;
    lat_drop: number;
    lng_drop: number;
    zoom = 10;
    inputAddress;
    inputAddress_drop;
    format;
    date;
    paymentType;
    progress;
    progress$;
    progressObserver;
    file1: File;
    Picture: string = "";
    deliverycharge = 0;
    paymentMethod = ['COD','DEBIT/CREDIT'];
    shippingOptions = ['INSTANT', '4 HOURS', 'SAME DAY'];
    btn_index;
    btn_index_ship;
    charge_index = 0;
    pickAddress ='hi'
    dropAddress = 0;
    drop=[]
    length_arr;
    payment: boolean = false;
    constructor(@Attribute('format') format, private _cookieService: CookieService,
    private router: Router,
    private ngZone: NgZone, private zone: NgZone,
    private http: Http,
    private _ApiMessageService: ApiMessageService,
    private _errorService:ErrorService,
     private ImageUploadModule: ImageUploadModule) {
         this.format = format;
    this.date =  new Date();
     }
     resizeOptions: ResizeOptions = {
        resizeMaxHeight: 600,
        resizeMaxWidth: 800
    };
    selected(imageResult: ImageResult) {
        this.Picture = imageResult.resized
            && imageResult.resized.dataURL
            || imageResult.dataURL;
        this.file1 = imageResult.file;
    }
    ngOnInit() {
        this.UserName = this._cookieService.get('ez_UserName')
        var autocomplete:any;
        var options = { componentRestrictions: {country: "IN"} };
    this. inputAddress = document.getElementById('address');
    autocomplete = new google.maps.places.Autocomplete(this.inputAddress,options)
    google.maps.event.addListener(autocomplete, 'place_changed', ()=> {

        this.ngZone.run(() => {
           this.zoom=8;
          var place = autocomplete.getPlace();
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        //this.map_popup = true;
        });

    });
     var autocomplete_drop:any;
     var options = { componentRestrictions: {country: "IN"} };
    this. inputAddress_drop = document.getElementById('address_drop');
    autocomplete_drop = new google.maps.places.Autocomplete(this.inputAddress_drop,options)
    google.maps.event.addListener(autocomplete_drop, 'place_changed', ()=> {

        this.ngZone.run(() => {
           this.zoom=8;
          var place = autocomplete_drop.getPlace();
          this.lat_drop = place.geometry.location.lat();
          this.lng_drop = place.geometry.location.lng();
        //this.map_popup = true;
        });

    });
}
 centers($event){
let pos = ( $event);
this.lat=pos.lat;
this.lng=pos.lng;

}
pos_pick($event){
    let pos = ( $event);
    this.lat=pos.coords.lat;
    this.lng=pos.coords.lng;
    console.log("pick "+this.lat+','+this.lng);
}
pos_drop($event){
    let pos = ( $event);
    this.lat_drop=pos.coords.lat;
    this.lng_drop=pos.coords.lng;
    console.log("drop "+this.lat_drop+','+this.lng_drop);
}
onErrorHandled() {
    this.map_popup = false;
}
payment_index(i3) {
    let index = i3;
    if(index==this.btn_index) {
        this.paymentType = this.btn_index+1;
        return "#0b97c4"
    }
}
payments(ii) {
    this.btn_index = ii;
    if(this.btn_index==0) {
        return "#0b97c4"
    } else if(this.btn_index==1) {
        return "#0b97c4"
    } else if(this.btn_index==2) {
        return "#0b97c4"
    }
}
shipping_index(shipi) {
    let index = shipi;
    if(index==this.btn_index_ship) {
        this.charge_index = this.btn_index_ship+1;
        return "#31708f"
    }
}
shipping_indexs(ship) {
    this.btn_index_ship = ship;
    if(this.btn_index_ship==0) {
        this.deliverycharge = 548;
        return "#31708f"
    } else if(this.btn_index_ship==1) {
        this.deliverycharge = 450;
        return "#31708f"
    } else if(this.btn_index_ship==2) {
        this.deliverycharge = 250;
        return "#31708f"
    }
}
onSubmit(form: NgForm) {
//    let pick = form.value.pickAddress;
//    let pick1 = form.value.dropAddress;
//   let pick2 = form.value.itemName;
//    let pick3 = form.value.receiverName;
//    let pick4 = form.value.receiverPhone;
//    this.drop=[]
   this.drop.push({pickAddress:form.value.pickAddress,dropAddress:form.value.dropAddress,itemName:form.value.itemName,receiverName:form.value.receiverName,receiverPhone:form.value.receiverPhone})
   this.drop=this.drop.reverse()
    this.length_arr = this.drop.length;
    form.resetForm()
    // this.length_arr=this.length_arr+'orders'
//    drop.push(this.pickAddress)
//    console.log("drop withou Json"+(this.drop))
//   console.log("drop "+JSON.stringify(this.drop))


}
submit(){
this.payment= true;

}
remove(item) {
   this.drop.splice(this.drop.indexOf(item),1);
   this.length_arr = this.drop.length;
}

}
