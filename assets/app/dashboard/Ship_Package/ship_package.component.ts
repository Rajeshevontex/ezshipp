import { NgForm } from '@angular/forms';
import { OrderModel } from './../../front_end_models/orderModel';
import { ImageResult, ResizeOptions, ImageUploadModule } from 'ng2-imageupload';
import { ErrorService } from './../../errors/error.service';
import { ApiMessageService } from './../../authentication/apimessages.service';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { Component, OnInit, NgZone, Attribute } from '@angular/core';

@Component({
    selector: 'app-ship_package',
    templateUrl: './ship_package.component.html',
    styleUrls: ['./ship_package.component.css']
})

export class ShipPackageComponent implements OnInit {
    location: Location;
    imageSearch = "./images/search.png";
    imageBag = "./images/Bag.png";
    imagemap = "./images/map.png";
    imageusericon = "./images/usericon.png";
    imageupload = "./images/upload.png";
    map_popup:boolean = false;
    UserName;
    url:string = '';
    imagecall = "./images/call.png";
    title: string = 'My first angular2-google-maps project';
    lat: number;
    lng: number;
    lat_drop: number;
    lng_drop: number;
    zoom = 10;
    inputAddress;
    inputAddress_drop;
    format;
    date;
    paymentType;
    progress;
    progress$;
    progressObserver;
    file1: File;
    Picture: string = "";
    deliverycharge = 0;
    paymentMethod = ['COD','DEBIT / CREDIT'];
    shippingOptions = ['INSTANT', '4 HOURS', 'SAME DAY'];
    btn_index;
    btn_index_ship;
    charge_index = 0;
    buildname;
    bnam;
    pickaddres;
    drop_addres;
    succ:boolean = false;
    msgs;
    order_History=[]
    issucess:boolean=false;
    description;bookingType;receiverPhone;name;pickAddress;dropAddress;receiverName;
    constructor(@Attribute('format') format, private _cookieService: CookieService,
    private router: Router,
    private ngZone: NgZone, private zone: NgZone,
    private http: Http,
    private _ApiMessageService: ApiMessageService,
    private _errorService:ErrorService,
     private ImageUploadModule: ImageUploadModule) {
         this.format = format;
    this.date =  new Date();
     }
     resizeOptions: ResizeOptions = {
        resizeMaxHeight: 600,
        resizeMaxWidth: 800
    };
    selected(imageResult: ImageResult) {
        this.Picture = imageResult.resized
            && imageResult.resized.dataURL
            || imageResult.dataURL;
        this.file1 = imageResult.file;
    }
    ngOnInit() {
        this.UserName = this._cookieService.get('ez_UserName')
        this.orderHistory()
        var autocomplete:any;
        var options = { componentRestrictions: {country: "IN"} };
    this. inputAddress = document.getElementById('address');
    autocomplete = new google.maps.places.Autocomplete(this.inputAddress,options)
    google.maps.event.addListener(autocomplete, 'place_changed', ()=> {

        this.ngZone.run(() => {
           this.zoom=8;
          var place = autocomplete.getPlace();
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
        //this.map_popup = true;
        this.getGeoLocation(this.lat,this.lng);
        });

    });
     var autocomplete_drop:any;
     var options = { componentRestrictions: {country: "IN"} };
    this. inputAddress_drop = document.getElementById('address_drop');
    autocomplete_drop = new google.maps.places.Autocomplete(this.inputAddress_drop,options)
    google.maps.event.addListener(autocomplete_drop, 'place_changed', ()=> {

        this.ngZone.run(() => {
           this.zoom=8;
          var place = autocomplete_drop.getPlace();
          this.lat_drop = place.geometry.location.lat();
          this.lng_drop = place.geometry.location.lng();
          this.getGeoLocation_drop(this.lat_drop,this.lng_drop);
        //this.map_popup = true;
        //console.log("drop "+this.lat_drop+" "+this.lng_drop);
        });

    });
    this.setCurrentPosition();

}
private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }
 centers($event){
let pos = ( $event);
this.lat=pos.lat;
this.lng=pos.lng;
}
pos_pick($event){
    let pos = ( $event);
    this.lat=pos.coords.lat;
    this.lng=pos.coords.lng;
}
pos_drop($event){
    let pos = ( $event);
    this.lat_drop=pos.coords.lat;
    this.lng_drop=pos.coords.lng;
}
onErrorHandled() {
    this.map_popup = false;
}
payment_index(i3) {
    let index = i3;
    if(index==this.btn_index) {
        this.paymentType = this.btn_index+1;
        return "#0b97c4"
    }
}
payments(ii) {
    this.btn_index = ii;
    if(this.btn_index==0) {
        return "#0b97c4"
    } else if(this.btn_index==1) {
        return "#0b97c4"
    } else if(this.btn_index==2) {
        return "#0b97c4"
    }
}
shipping_index(shipi) {
    let index = shipi;
    if(index==this.btn_index_ship) {
        this.charge_index = this.btn_index_ship+1;
        return "#31708f"
    }
}
shipping_indexs(ship) {
    this.btn_index_ship = ship;
    if(this.btn_index_ship==0) {
        this.deliverycharge = 199;
        return "#31708f"
    } else if(this.btn_index_ship==1) {
        this.deliverycharge = 129;
        return "#31708f"
    } else if(this.btn_index_ship==2) {
        this.deliverycharge = 99;
        return "#31708f"
    }
}
show_map() {
    this.map_popup = true;
    console.log("Clicking");
}

getGeoLocation(lat: number, lng: number) {
if (navigator.geolocation) {
    let geocoder = new google.maps.Geocoder();
    let latlng = new google.maps.LatLng(lat, lng);
    let request = { latLng: latlng };

    geocoder.geocode(request, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        let result = results[0];
        let rsltAdrComponent = result.address_components;
        let resultLength = rsltAdrComponent.length;
        if (result != null) {
          this.buildname = rsltAdrComponent[resultLength-8].short_name;
          let pickadd = results[0].formatted_address;
          this.pickaddres = pickadd;

          this.bnam = rsltAdrComponent[resultLength-7].short_name;
        } else {
          alert("No address available!");
        }
      }
    });
}
}

getGeoLocation_drop(lat: number, lng: number) {
if (navigator.geolocation) {
    let geocoder = new google.maps.Geocoder();
    let latlng = new google.maps.LatLng(lat, lng);
    let request = { latLng: latlng };

    geocoder.geocode(request, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        let result = results[0];
        let rsltAdrComponent = result.address_components;
        let resultLength = rsltAdrComponent.length;
        if (result != null) {
          this.buildname = rsltAdrComponent[resultLength-8].short_name;
          let pickadd = results[0].formatted_address;
          this.drop_addres = pickadd;
          this.bnam = rsltAdrComponent[resultLength-7].short_name;
        } else {
          alert("No address available!");
        }
      }
    });
}
}

onSubmit(form: NgForm) {
    let pick = form.value.pickAddress;
    let drop = form.value.dropAddress;
    let uid = this._cookieService.get('ez_cusID')
    let itm_name = form.value.itemName;
    let rec_phone = form.value.receiverPhone;
    let recv_name = form.value.receiverName;
    let recv_desc = form.value.itemDescription;
    //this.getGeoLocation(this.lat,this.lng);
    this.getGeoLocation(this.lat_drop,this.lng_drop);
    let ordertype = 1;
    //console.log("val "+pick+" "+drop+" "+uid+" "+itm_name+" "+rec_phone+" "+recv_name);
    const OrderModelBody = new OrderModel(uid,this.pickaddres,this.drop_addres,this.lat,this.lng,this.lat_drop,this.lng_drop,itm_name,recv_desc,this.date,ordertype,this.charge_index,recv_name,rec_phone,this.paymentType,this.deliverycharge)
    const body = JSON.stringify(OrderModelBody)
    const headers = new Headers({ 'Content-Type': 'application/json' })
    return this.http.post(this.url+'/Order_Anything', body, { headers: headers })
        .subscribe(
        data => {
            if (data.json().success) {
                this.msgs = data.json().extras.Status;
                // this.succ = true;
                this.issucess=true
                setTimeout(function () {
                    // this.succ = false;
                    this.issucess=false
                    this.orderHistory()
                }.bind(this), 5000);
form.resetForm()
            } else {
                const msgNumber: number = parseInt(data.json().extras.msg);
                let message = this._ApiMessageService.ApiMessages[msgNumber]
                this._errorService.handleError(message)
            }
        }
        )
}
orderHistory(){
      let uid = this._cookieService.get('ez_cusID')
        const body = new OrderModel(uid)
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(this.url + '/User_Order_History', body, { headers: headers })
            .subscribe(
            data => {
                if (data.json().success) {
                    this.order_History=data.json().extras.OrderData
                    //console.log("Order values "+JSON.stringify(data.json().extras.OrderData));
                } else {
                    const msgNumber: number = parseInt(data.json().extras.msg);
                    let message = this._ApiMessageService.ApiMessages[msgNumber]
                    this._errorService.handleError(message)
                }
            }
            )
}
repeat(name,pickaddres,drop_addres,receiverPhone,paymentType,description,bookingType,receiverName) {
this.name = name;
this.pickAddress = pickaddres;
this.dropAddress = drop_addres;
this.receiverPhone = receiverPhone;
this.paymentType = paymentType;
this.description = description;
this.bookingType = bookingType;
this.receiverName = receiverName;
let pay = this.paymentType - 1;
this.payment_index(pay);
this.payments(pay);
let booking = this.bookingType - 1;
this.shipping_indexs(booking);
this.shipping_index(booking);
//console.log("repeat val "+this.name+" "+this.pickAddress+" "+this.dropAddress+" "+this.receiverPhone+" "+this.paymentType+" "+this.description+" "+this.bookingType+" "+this.receiverName);
}
onSubmit_Payment(form: NgForm) {
    // let key = "rzp_test_mHybvn4OUmX7Qw";
    // let amount = "100";
    // let name = "Raju";
    // let handler = response.razorpay_payment_id
}

}
