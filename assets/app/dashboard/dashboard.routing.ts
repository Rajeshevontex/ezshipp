import { PayComponent } from './pay/pay.component';
import { BulkOrderComponent } from './bulk_order/bulk_order.component';
import { OrderHistoryComponent } from './order-history/order_history.component';
import { ShipPackageComponent } from './Ship_Package/ship_package.component';
import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from '@angular/core';

export const DASHBOARD_ROUTES: Routes = [
    { path: '', redirectTo:'ship_package', pathMatch: 'full' },
    { path: 'ship_package', component: ShipPackageComponent },
    { path: 'order_history', component: OrderHistoryComponent },
    { path: 'bulk_order', component: BulkOrderComponent },
    { path: 'payment', component: PayComponent } 
];
//export const DashboardRouting: ModuleWithProviders = RouterModule.forChild(DASHBOARD_ROUTES);
