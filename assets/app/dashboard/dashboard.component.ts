import { ErrorService } from './../errors/error.service';
import { ApiMessageService } from './../authentication/apimessages.service';
import { OrderModel } from './../front_end_models/orderModel';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { Component, OnInit, NgZone, Attribute } from '@angular/core';
import { MapsAPILoader } from 'angular2-google-maps/core';
import { GoogleMapsAPIWrapper, InfoWindowManager, MarkerManager } from 'angular2-google-maps/core';
import { Http, Headers } from '@angular/http';
import { ImageResult, ResizeOptions, ImageUploadModule } from 'ng2-imageupload';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
   UserName;
   imageLogo = './images/logo-blue-1.png';
   buildname
   bnam
    constructor(@Attribute('format') format, private _cookieService: CookieService,
    private router: Router,
    private ngZone: NgZone, private zone: NgZone,
    private http: Http,
    private _ApiMessageService: ApiMessageService,
    private _errorService:ErrorService,
     private ImageUploadModule: ImageUploadModule) {
    
}

    ngOnInit() {
         this.UserName = this._cookieService.get('ez_UserName')
         this._cookieService.remove('ez_Password')
}

    logout() {
        this._cookieService.removeAll()
        this.router.navigateByUrl('/signin')
    }
   
}