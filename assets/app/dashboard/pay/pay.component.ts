import { NgForm } from '@angular/forms';
import { OrderModel } from './../../front_end_models/orderModel';
import { ImageResult, ResizeOptions, ImageUploadModule } from 'ng2-imageupload';
import { ErrorService } from './../../errors/error.service';
import { ApiMessageService } from './../../authentication/apimessages.service';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { Component, OnInit, NgZone, Attribute } from '@angular/core';


@Component({
    selector: 'app-pay',
    templateUrl: './pay.component.html',
    styles: [`

    `]
})

export class PayComponent implements OnInit {
    format;
    date;
    
     constructor(@Attribute('format') format, private _cookieService: CookieService,
    private router: Router,
    private ngZone: NgZone, private zone: NgZone,
    private http: Http,
    private _ApiMessageService: ApiMessageService,
    private _errorService:ErrorService,
     private ImageUploadModule: ImageUploadModule) {
         this.format = format;
    this.date =  new Date();
     }
    ngOnInit() {

    }
    pay() {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.get('https://api.razorpay.com/v1/payments')
            .subscribe(
            data => {
                if (data.json().success) {
                    console.log("Success");
                } else {
                    const msgNumber: number = parseInt(data.json().extras.msg);
                    let message = this._ApiMessageService.ApiMessages[msgNumber]
                    this._errorService.handleError(message)
                }
            }
            )
    }
 payment() {
      var RazorpayCheckout: any;
      var Razorpay:any;
     var options = {
        "key": "rzp_test_mHybvn4OUmX7Qw",
        "amount": "100", // 2000 paise = INR 20
        "name": "Srinivas",
        "description": "Purchase Description",
        "handler": function(response) {
            alert(response.razorpay_payment_id);
        },
        "prefill": {
            "name": "Raju",
            "email": "rajesh.keerthi@evontex.com"
        },
        "notes": {
            "address": "Hello World"
        },
        "theme": {
            "color": "#F37254"
        }
    };
     var rzp1 = new Razorpay(options);
     rzp1.open();
     console.log("OK");
 }
}