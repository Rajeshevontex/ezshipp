import { ForgotPasswordComponent } from './forgotPassword/forgotPassword.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { SigninComponent } from './signin/signin.component';
import { AuthRouting } from './authentication.routing';
import { SignupComponent } from './signup/signup.component';

import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";

@NgModule({
    declarations: [
        SignupComponent,
        SigninComponent,
        WelcomeComponent,
        ForgotPasswordComponent
    ],


    imports: [
        CommonModule,
        FormsModule,
        AuthRouting
    ],
})
export class AuthenticationModule {

}