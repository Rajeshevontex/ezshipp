import { ErrorService } from './../../errors/error.service';
import { ApiMessageService } from './../apimessages.service';
import { Http, Headers } from '@angular/http';
import { AuthenticationModel } from './../../front_end_models/authenticationModel';
import { CookieService } from 'angular2-cookie/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {
    url:string = '';
    signup: boolean = false;
    signup_phone:boolean = false;
    sign_up:boolean = false;
    signup_otp:boolean = false;
    Phonenumber;
    country_code;
    error:boolean = false;
    reg_suc:boolean = false;
    fullName;
    welcome:boolean = false;
    pwd;
    Phone;
    loading:boolean = false;
    data:boolean = false;
    constructor(private router: Router, private _cookieService: CookieService, private http: Http, private _ApiMessageService: ApiMessageService,
    private _errorService:ErrorService) {}
   ngOnInit() {
       this.signup_phone = true;
       this.loading = true;
   setTimeout(function () {
    this.loading = false;
    this.data = true;
}.bind(this), 5000);
   }
   onSubmit_Phone(form: NgForm) {
       this.country_code = "+91";
       this.Phonenumber = form.value.Phone;
       let phone = this._cookieService.put('PhoneNumber', this.Phonenumber, 1);
       //console.log("phone with " + country_code+" "+phone_val);
       //form.resetForm();
       if(this.Phonenumber == '') {
           console.log("Phone Null");
       } else {
       this.gen_otp();  
       }
       
   }
   gen_otp() {
       let cv = this._cookieService.get('Auth');
       const body = new AuthenticationModel(cv,this.Phonenumber,this.country_code)
    //    console.log("body val "+JSON.stringify(body));
            const headers = new Headers({ 'Content-Type': 'application/json' });
            return this.http.post(this.url + '/GenerateOTP', body, { headers: headers })
                .subscribe(
                data => {
                    if (data.json().success) {
                        // console.log("success "+data.json().extras.Status)
                        this.signup_otp = true;
                        this.signup_phone = false;
                    } else {
                        const msgNumber: number = parseInt(data.json().extras.msg);
                        let message = this._ApiMessageService.ApiMessages[msgNumber]
                        this._errorService.handleError(message)
                    }
                }
                )
   }
   onSubmit_OTP(form: NgForm) {
       let otp_val = form.value.OTP;
       if(otp_val=='') {
           console.log("OTP Null");
       } else {
           
       let ph = this._cookieService.get('PhoneNumber');
       const body = new AuthenticationModel(null,ph,this.country_code,otp_val)
       //console.log("body val "+JSON.stringify(body));
            const headers = new Headers({ 'Content-Type': 'application/json' });
            return this.http.post(this.url + '/ValidateLatestOTP', body, { headers: headers })
                .subscribe(
                data => {
                    if (data.json().success) {
                        //console.log("success "+data.json().extras.Status)
                        this._cookieService.put('OTP', data.json().extras.OTP, 1)
                        let code = this._cookieService.get('OTP')
                        //console.log("otp res value "+ code);
                        this.signup_otp = false;
                        this.sign_up = true;
                    } else {
                        const msgNumber: number = parseInt(data.json().extras.msg);
                        let message = this._ApiMessageService.ApiMessages[msgNumber]
                        this._errorService.handleError(message)
                    }
                }
                )
    //                     this.signup_otp = false;
    //    this.sign_up = true;
       }
   }
   back_phone() {
       this.signup_otp = false;
       this.signup_phone = true;
   }
   onSubmit(form: NgForm) {
       this.error = false;
       this.fullName = form.value.Name;
       let Email = form.value.Email;
       this.Phone = form.value.Phonenumber;
       this.pwd = form.value.Password;
       let chck = form.value.tc;
       this._cookieService.put('ez_UserName',this.fullName, 1)
       this._cookieService.put('ez_Password',this.pwd)
       if(chck=='') {
           this.error = true;
       } else {
           //console.log("body values "+names+" "+Email+" "+Phone+" "+pwd+" "+chck);
           let ch = 1;
              let code = this._cookieService.get('OTP')
       const body = new AuthenticationModel(null,this.Phone,this.country_code,null,code,this.fullName,Email,this.pwd,ch)
       //console.log("Registration body val "+JSON.stringify(body));
            const headers = new Headers({ 'Content-Type': 'application/json' });
            return this.http.post(this.url + '/Customer_Sign_Up', body, { headers: headers })
                .subscribe(
                data => {
                    if (data.json().success) {
                        //console.log("success "+data.json().extras.Status);
                        form.resetForm();
                        this.router.navigateByUrl('/welcome')
                        // this.welcome = true;
                        // this.signin_direct();
                    } else {
                        const msgNumber: number = parseInt(data.json().extras.msg);
                        let message = this._ApiMessageService.ApiMessages[msgNumber]
                        this._errorService.handleError(message)
                    }
                }
                )
       }
       
   }

   signin() {
       this.router.navigateByUrl('/signin');
   }
   signin_direct() {
       const body = new AuthenticationModel(null,this.Phone,null,null,null,null,null,this.pwd)
            const headers = new Headers({ 'Content-Type': 'application/json' });
            return this.http.post(this.url + '/Customer_Signin', body, { headers: headers })
                .subscribe(
                data => {
                    if (data.json().success) {
                        //console.log("success "+data.json().extras.Status+" "+data.json().extras.CustomerData.CustomerID)
                        this._cookieService.put('ez_cusID', data.json().extras.CustomerData.CustomerID, 1)
                        let id = this._cookieService.get('ez_cusID')
                        //console.log("id "+id)
                        this.welcome = false;
                        this.router.navigateByUrl('/dashboard')
                    } else {
                        const msgNumber: number = parseInt(data.json().extras.msg);
                        let message = this._ApiMessageService.ApiMessages[msgNumber]
                        this._errorService.handleError(message)
                    }
                }
                )
   }
}