import { ErrorService } from './../../errors/error.service';
import { ApiMessageService } from './../apimessages.service';
import { AuthenticationModel } from './../../front_end_models/authenticationModel';
import { CookieService } from 'angular2-cookie/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';

import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.css']
})

export class WelcomeComponent implements OnInit {
    UserName;
    Pwd;
    ph;
    url:string = '';
    constructor(private router: Router,
    private _cookieService: CookieService,
    private http: Http,
    private _ApiMessageService: ApiMessageService,
    private _errorService:ErrorService) {}
    ngOnInit() {
        this.UserName = this._cookieService.get('ez_UserName')
        this.ph = this._cookieService.get('PhoneNumber')
        this.Pwd = this._cookieService.get('ez_Password')
        this.signin_direct();
    }
    signin_direct() {
       const body = new AuthenticationModel(null,this.ph,null,null,null,null,null,this.Pwd)
            const headers = new Headers({ 'Content-Type': 'application/json' });
            return this.http.post(this.url + '/Customer_Signin', body, { headers: headers })
                .subscribe(
                data => {
                    if (data.json().success) {
                        //console.log("success "+data.json().extras.Status+" "+data.json().extras.CustomerData.CustomerID)
                        this._cookieService.put('ez_cusID', data.json().extras.CustomerData.CustomerID)
                        let id = this._cookieService.get('ez_cusID')
                        //console.log("id "+id)
                        this.router.navigateByUrl('/dashboard')
                    } else {
                        const msgNumber: number = parseInt(data.json().extras.msg);
                        let message = this._ApiMessageService.ApiMessages[msgNumber]
                        this._errorService.handleError(message)
                    }
                }
                )
   }
}