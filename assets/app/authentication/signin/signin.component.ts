import { ErrorService } from './../../errors/error.service';
import { ApiMessageService } from './../apimessages.service';
import { Http, Headers } from '@angular/http';
import { AuthenticationModel } from './../../front_end_models/authenticationModel';
import { CookieService } from 'angular2-cookie/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css']
})

export class SigninComponent implements OnInit {
    url:string = '';
    loading:boolean = false;
    data:boolean = false;
    isFirstTimeLogin:boolean=false
   constructor(private router: Router,
    private http: Http,
    private _ApiMessageService: ApiMessageService,
    private _errorService:ErrorService,
    private _cookieService: CookieService) {}
   ngOnInit() {
       if(this._cookieService.get('ez_cusID')==null) {
    this.router.navigateByUrl('/signin')
}else{
    this.router.navigateByUrl('/dashboard')
}
this.loading = true;
   setTimeout(function () {
    this.loading = false;
    this.data = true;
}.bind(this), 5000);
   }

   signup_call() {
       this.router.navigateByUrl('/signup');
   }
   forgot_call(){
       this.router.navigateByUrl('/forgot');
   }
   onSubmit(form: NgForm) {
       let phne = form.value.Phone;
       let pwd = form.value.Password;
       //console.log("values "+phne+" "+pwd);
       const body = new AuthenticationModel(null,phne,null,null,null,null,null,pwd)
       //console.log("body val "+JSON.stringify(body));
            const headers = new Headers({ 'Content-Type': 'application/json' });
            return this.http.post(this.url + '/Customer_Signin', body, { headers: headers })
                .subscribe(
                data => {
                    if (data.json().success) {
                        //  console.log("CustomerData "+JSON.stringify(data.json().extras.CustomerData))
                        // console.log("val "+JSON.stringify(data.json().extras.CustomerData.First_Time_Login))
                          if (data.json().extras.CustomerData.First_Time_Login){
                              this._cookieService.put('ez_cusID', data.json().extras.CustomerData.CustomerID, 1)
                                this._cookieService.put('ez_UserName', data.json().extras.CustomerData.First_name, 1)
                                let id = this._cookieService.get('ez_cusID')
                            this.isFirstTimeLogin=true
                          }else{
                                this._cookieService.put('ez_cusID', data.json().extras.CustomerData.CustomerID, 1)
                                this._cookieService.put('ez_UserName', data.json().extras.CustomerData.First_name, 1)
                                let id = this._cookieService.get('ez_cusID')
                                //console.log("id "+id)
                                 this.router.navigateByUrl('/dashboard')
                          }
                        //console.log("success "+data.json().extras.Status+" "+data.json().extras.CustomerData.CustomerID)

                    } else {
                        const msgNumber: number = parseInt(data.json().extras.msg);
                        // console.log("Error"+msgNumber);
                        let message = this._ApiMessageService.ApiMessages[msgNumber]
                        this._errorService.handleError(message)
                    }
                }
                )
   }
   onSubmit_firsttime(form:NgForm){
       let cust_Id =this._cookieService.get('ez_cusID')
       let ph = form.value.Password;
        let ph1 = form.value.Confirm_Password;

       const body = new AuthenticationModel()
       if(ph==ph1){
 const body = new AuthenticationModel(null,null,null,null,null,null,null,ph,null,cust_Id)
        const headers = new Headers({ 'Content-Type': 'application/json' });
            return this.http.post(this.url + '/First_Time_Changed_Customer_Password', body, { headers: headers })
                .subscribe(
                data => {
                    if (data.json().success) {
                        // console.log("val "+JSON.stringify(data.json().extras.Status))
                        //   if (data.json().extras.FirstTimeLogin){
                        //     this.isFirstTimeLogin=true
                        //   }
                        // console.log("success "+data.json().extras.Status+" "+data.json().extras.CustomerData.CustomerID)
                        // this._cookieService.put('ez_cusID', data.json().extras.CustomerData.CustomerID)
                        // this._cookieService.put('ez_UserName', data.json().extras.CustomerData.First_name)
                        // let id = this._cookieService.get('ez_cusID')
                        //console.log("id "+id)
                         this.router.navigateByUrl('/dashboard')
                    } else {
                        const msgNumber: number = parseInt(data.json().extras.msg);
                        // console.log("Error"+msgNumber);
                        let message = this._ApiMessageService.ApiMessages[msgNumber]
                        this._errorService.handleError(message)
                    }
                }
                )

    }else{
        alert("confirm password doesn't match check again")
    }
       //console.log("body val "+JSON.stringify(body));

   }
}