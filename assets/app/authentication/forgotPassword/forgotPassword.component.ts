import { AuthenticationModel } from './../../front_end_models/authenticationModel';
import { ErrorService } from './../../errors/error.service';
import { ApiMessageService } from './../apimessages.service';
import { Http, Headers } from '@angular/http';

import { CookieService } from 'angular2-cookie/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
@Component({
    selector:'app-forgot',
    templateUrl:"./forgotPassword.component.html",
    styleUrls:["../signup/signup.component.css"]
})
export class ForgotPasswordComponent implements OnInit{
    constructor(private router: Router,
    private http: Http,
    private _ApiMessageService: ApiMessageService,
    private _errorService:ErrorService,
    private _cookieService: CookieService) {}
    url=''
    sucess:boolean=false
    isloading=false
    isdata=false
    ngOnInit() {
        this.isloading = true;
   setTimeout(function () {
    this.isloading = false;
    this.isdata = true;
}.bind(this), 5000);

    }
onSubmit_Phone(form:NgForm){
     var country_code = "+91";
 const body = new AuthenticationModel(null,form.value.Phone,country_code)
 const headers = new Headers({ 'Content-Type': 'application/json' });
            return this.http.post(this.url + '/Customer_Forgot_Password', body, { headers: headers })
                .subscribe(
                data => {
                    if (data.json().success) {
                        // console.log("sucess")
                         this.router.navigateByUrl('/signin')
                    } else {
                        const msgNumber: number = parseInt(data.json().extras.msg);
                        console.log("Error"+msgNumber);
                        let message = this._ApiMessageService.ApiMessages[msgNumber]
                        this._errorService.handleError(message)
                    }
                }
                )
   }
}
