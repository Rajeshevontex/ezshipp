import { ForgotPasswordComponent } from './forgotPassword/forgotPassword.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { RouterModule, Routes } from '@angular/router';

const AUTH_ROUTES: Routes = [
    { path: '', redirectTo: '/signin', pathMatch: 'full' },
    { path: 'signin', component: SigninComponent},
    { path: 'signup', component: SignupComponent },
    { path: 'forgot', component: ForgotPasswordComponent },
    { path: 'welcome', component: WelcomeComponent }
];
export const AuthRouting = RouterModule.forChild(AUTH_ROUTES);
