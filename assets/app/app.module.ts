import { PayComponent } from './dashboard/pay/pay.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { Dummycomponent } from './dummy.component';
import { BulkOrderComponent } from './dashboard/bulk_order/bulk_order.component';
import { OrderHistoryComponent } from './dashboard/order-history/order_history.component';
import { ShipPackageComponent } from './dashboard/Ship_Package/ship_package.component';
import { AuthGuard } from './authentication/auth.guard';
import { ApiMessageService } from './authentication/apimessages.service';
import { AuthenticationModule } from './authentication/authentication.module';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { routing } from './app.routing';
import { ErrorComponent } from './errors/error.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CookieService, CookieOptions, BaseCookieOptions } from 'angular2-cookie/core';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { AppComponent } from "./app.component";
import { ErrorService } from './errors/error.service';
import { ResizeOptions, ImageResult, ImageUploadModule } from 'ng2-imageupload';
// import { ForgotPasswordComponent } from "./authentication/forgotPassword/forgotPassword.component";

export function cookieServiceFactory() {
  return new CookieService();
}

@NgModule({
    declarations: [
        AppComponent,
        ErrorComponent,
        DashboardComponent,
        Dummycomponent
    ],
    imports: [
        BrowserModule,
        routing,
        FormsModule,
        HttpModule,
        AuthenticationModule,
       DashboardModule,
       ImageUploadModule,
        AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCHAAF-2jzmx-G06WCapwMZK0VG3pIC3yY'
    })

    ],
    providers: [
        { provide: CookieService, useFactory: cookieServiceFactory },
        ApiMessageService,
        ErrorService,
        AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}