import { AuthenticationModel } from './front_end_models/authenticationModel';
import { CookieService } from 'angular2-cookie/core';
import { Component, OnInit } from '@angular/core';
import { Http, Headers } from "@angular/http";
import { Router } from '@angular/router';

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    constructor(private _cookieService: CookieService, private http: Http, private router: Router) {}
    isloading:boolean = false;
    url:string = '';
    ngOnInit() {
        let ph = this._cookieService.get('Auth');
        // console.log("cookie "+ph);
        if(ph==undefined) {
            // console.log("Null Cookie Value /Cookie_Creation");
            const body = new AuthenticationModel(null)
            const headers = new Headers({ 'Content-Type': 'application/json' });
            return this.http.get(this.url+'/Cookie_Creation')
                .subscribe(
                data => {
                    if(data.json().success) {
                    // console.log("Success");
                    // console.log("get calling "+JSON.stringify(data.json().extras));
                    let cvalue = this._cookieService.put('Auth', data.json().extras.Cookie, 1)
                    let cvalue1 = this._cookieService.get('Auth');
                    //console.log("value " + cvalue1);
                    } else {
                        console.log("Error");
                    }
                    
                }
                )
        } else {
            // console.log("Cookie Value " + ph);
            //this.isloading = true;
        }
        //console.log("App Component Entering");
    }
}