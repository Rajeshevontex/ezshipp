import './polyfills';
import { platformBrowser } from "@angular/platform-browser";
import { enableProdMode } from "@angular/core";

import { AppModuleNgFactory } from './app.module.ngfactory';
import { DashboardModuleNgFactory } from './dashboard/dashboard.module.ngfactory';


enableProdMode();

platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
platformBrowser().bootstrapModuleFactory(DashboardModuleNgFactory);