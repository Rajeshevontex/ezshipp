export class OrderModel{
    constructor(public  CustomerID?,
                public pickAddress?,
                public dropAddress?,
                public pickLatitude?,
                public pickLongitude?,
                public dropLatitude?,
                public dropLongitude?,
                public itemName?,
                public itemDescription?,
                public order_datetime?,
                public orderType?,
                public bookingType?,
                public receiverName?,
                public receiverPhone?,
                public paymentType?,
                public deliverycharge?
   ){}
}