'use strict';
module.exports = function(express, app, socket){

  var app = app;
  var router = app;
  var Config = require('../Config/config.js'); //Setting the Config Path
  var Socket;
  var io =socket;
  io.on('connection', function(socket){
  	Socket = socket;
  });

////////////////////////////////////////////////////////////////////////////////////////////////////
//                                Setting All the PATH's                                          //
////////////////////////////////////////////////////////////////////////////////////////////////////

  /*--------------------------------------Begin the Path's---------------------------------------------*/
  var Config = require('../Config/config.js'); //Setting the Config Path
  var customermod = require('../CoreModules/customermod.js'); // Setting the Path for Customer Modules
  var stripemod = require('../CoreModules/stripemod.js'); // Setting the Path for Stripe Modules
  var mailgunmod = require('../CoreModules/mailgunmod.js'); // Setting the Path for Mailgun Modules


  /*--------------------------------------End of Path's------------------------------------------------*/


  ////////////////////////////////////////////////////////////////////////////////////////////////////
//                                     Customer API's                                              //
////////////////////////////////////////////////////////////////////////////////////////////////////

  /*--------------------------------------Begin of API's---------------------------------------------*/

//This Api use to Display User Order History
  app.post('/User_Order_History',function(req,res){
    var CustomerMod = new customermod();
      CustomerMod.Check_for_CustomerID(req.body,function (err,responcer) {
          if (err) {
              res.send(JSON.stringify(responcer));
          } else {
              CustomerMod.Find_USER_ORDER_DETAILS(req.body, function (err, Result) {
                  res.send(JSON.stringify(Result));
              })
          }
      })
  });

  //This API to ORDER ANYTHING
  app.post('/Order_Anything',function (req,res) {
    var CustomerMod = new customermod();
    CustomerMod.Check_Validity_Orders_Anything(req.body,function (err,responcer) {
      if (err) {
        res.send(JSON.stringify(responcer));
      } else {
        CustomerMod.Find_and_Update_Order_Sequence(function (err,SequenceNumber) {
          CustomerMod.Storing_Customer_Order_Details(req.body,SequenceNumber,function (err,Result1) {
            var OrderData = Result1.extras.OrderDetails;
            CustomerMod.Zone_Finding_of_Order(req.body,OrderData,function (err,Result2) {
              if(err){
                res.send(JSON.stringify(Result2));
              }else {
                res.send(JSON.stringify(Result2));
              }
            })
          })
        });
      }
    })
  });

    //This Api use to  Changed Customer Password
    app.post('/Update_Customer_Password',function (req,res) {
        var CustomerMod = new customermod();
        CustomerMod.Check_for_CustomerID(req.body,function (err,responcer) {
            if (err) {
                res.send(JSON.stringify(responcer));
            } else {
                CustomerMod.Update_Customer_Password(req.body,function (err,Result) {
                    res.send(JSON.stringify(Result));
                })
            }
        })
    });

    //This Api use for First Time Changed Customer Password
    app.post('/First_Time_Changed_Customer_Password',function (req,res) {
        var CustomerMod = new customermod();
        CustomerMod.Check_for_CustomerID(req.body,function (err,responcer) {
            if (err) {
                res.send(JSON.stringify(responcer));
            } else {
                CustomerMod.Update_Customer_Password(req.body,function (err,Result) {
                    res.send(JSON.stringify(Result));
                })
            }
        })
    });

    //This Api use for forgot Password
    app.post('/Customer_Forgot_Password',function (req,res) {
        var CustomerMod = new customermod();
        CustomerMod.Check_Whether_PhoneNumber_Exist(req.body, function (err,CustomerData) {
            if (err) {
                res.send(JSON.stringify(CustomerData));
            } else {
                var CustomerID = CustomerData._id;
                var First_name = CustomerData.First_name;
                CustomerMod.findCustomerForgotPasswordTries(req.body, function (err,ForgotPasswordStatus) {
                    if (err) {
                        res.send(JSON.stringify(ForgotPasswordStatus));
                    } else {
                        CustomerMod.RegisterCustomerForgotPasswordTries(req.body,function (err,ForgotPasswordStore ) {
                            CustomerMod.GenerateRandomPasswordandUpdateItinSchema(req.body,CustomerID,First_name,function (err,Result) {
                                res.send(JSON.stringify(Result));
                                CustomerMod.DeleteCustomerPasswordTries(req.body,function (err,RemovePassword) {
                                    
                                })
                            })
                        })
                    }
                })
            }
        })
    });


//This Api use  for Login
  app.post('/Customer_Signin',function (req,res) {
    var CustomerMod = new customermod();
    CustomerMod.Check_Validity_Signin(req.body,function (err,responcer) {
      if(err){
        res.send(JSON.stringify(responcer));
      }else {
        CustomerMod.findCustomerPasswordTries(req.body, function (err,responcer) {
          if(err){
            res.send(JSON.stringify(responcer));
          }else {
            CustomerMod.Check_Whether_PhoneNumber_Exist(req.body, function (err,CustomerData) {
              if(err){
                res.send(JSON.stringify(CustomerData));
              }else {
                CustomerMod.CustomerSignIn(req.body,CustomerData,function (err,LoginStatus) {
                  if(err){
                    res.send(JSON.stringify(LoginStatus));
                  }else {
                    CustomerMod.Check_Customer_Session(CustomerData,function (err,SessionStatus) {
                      if(err){
                        CustomerMod.RegisteringCustomerSession(CustomerData,function (err,SessionData) {
                          res.send(JSON.stringify(SessionData));
                        })
                      }else {
                        CustomerMod.UpdatingCustomerSession(CustomerData,function (err,SessionData) {
                          res.send(JSON.stringify(SessionData));
                        })
                      }
                    })
                  }
                })
              }
            })
          }
        })
      }
    })
  });

  //This Api use to Create User Signup
  app.post('/Customer_Sign_Up',function (req,res) {
    var CustomerMod = new customermod();
    var StripeMod = new stripemod();
    var MailgunMod = new mailgunmod();

    CustomerMod.Check_Validity_Sign_Up(req.body,function (err,responcer) {
      if (err) {
        res.send(JSON.stringify(responcer));
      } else {
        CustomerMod.Check_Whether_Phone_Registered(req.body, function (err, responcer) {
          if (err) {
            res.send(JSON.stringify(responcer));
          } else {
            CustomerMod.Check_Whether_Email_Registered(req.body, function (err, responcer) {
              if (err) {
                res.send(JSON.stringify(responcer));
              } else {
                StripeMod.createcustomerstripe(req.body.Email,function (err,stripeId) {
                  var stripeId  = stripeId;
                  CustomerMod.FindtheCustomerSeqID(function (err, SeqLength) {
                    var CustomerLength = parseInt(SeqLength) + 1;
                    CustomerMod.UpdatetheCustomerSeqID(CustomerLength, function (err, SequenceUpdate) {
                      CustomerMod.Customer_Signup(req.body,CustomerLength,stripeId,function (err,Result) {
                        res.send(JSON.stringify(Result));
                        MailgunMod.sendEmailCustomerRegistration(req.body.Email,req.body.First_name,function (err,EmailStatus) {

                        })
                      });
                    })
                  })
                });
              }
            })
          }
        });
      }
    })
  });

  //This API use to Validate OTP of Latest
  app.post('/ValidateLatestOTP',function (req,res) {
    var CustomerMod = new customermod();
    CustomerMod.Check_Validity_Validating_OTP(req.body,function (err,responcer) {
      if (err) {
        res.send(JSON.stringify(responcer));
      } else {
        CustomerMod.ValidateLatestOTP(req.body,function (err,Result) {
          if (err) {
            res.send(JSON.stringify(Result));
          } else {
            res.send(JSON.stringify(Result));
            CustomerMod.RemoveLatestFalseOTPofPhone(req.body,function (err,responcer) {

            })
          }
        })
      }
    })
  });

  //This API use to Generate OTP
  app.post('/GenerateOTP',function (req,res) {
    var CustomerMod = new customermod();
    CustomerMod.FindCookiePhoneRegistered(req.body,function (err,responcer) {
      if (err) {
        res.send(JSON.stringify(responcer));
      } else {
        CustomerMod.Check_Validity_Generating_OTP(req.body, function (err, responcer) {
          if (err) {
            res.send(JSON.stringify(responcer));
          } else {
            CustomerMod.Check_Whether_Phone_Registered(req.body, function (err, responcer) {
              if (err) {
                res.send(JSON.stringify(responcer));
              } else {
                CustomerMod.UpdatePhoneOTPFalse(req.body,function (err,responcer) {
                  if (responcer.success) {
                    CustomerMod.CheckforPhoneNumberCount(req.body, function (err, responcer) {
                      if (err) {
                        res.send(JSON.stringify(responcer));
                      } else {
                        CustomerMod.GenerateOTPandStoreITinSchema(req.body, function (err, Result) {
                          res.send(JSON.stringify(Result));
                          if (Result.success) {
                            CustomerMod.RegisterCookiePhoneToken(req.body, function (err, CookiePhoneData) {

                            })
                          }
                        })
                      }
                    })
                  }
                });
              }
            })
          }
        });
      }
    })
  });

  //This API use to Generate Cookie for Security
  app.get('/Cookie_Creation',function (req,res) {
    var CustomerMod = new customermod();
    CustomerMod.GeneratingtheSessionCookie(function (err,Result) {
      res.send(JSON.stringify(Result));
    })
  });

  router.post('/',function(req,res){
    res.render('index');
  });


  /*--------------------------------------End of API's------------------------------------------------*/


};
