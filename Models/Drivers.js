var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Drivers = new Schema({
    driverseqId:String,
    name:String,
    lname:String,
    email:String,
    password:String,
    type_id:Number,
    acc_status:Number,
    phone:String,
    created_dt:String,
    last_active_dt:String,
    profilePic:String,
    depoId:String,
    lic_expiry_dt:String,
    license_pic:String,
    bank_passbook:String,
    bank_country:String,
    Account_num:String,
    Routing_num:String,
    bank_name:String,
    businessid:String,
    Devices:[{
        DeviceType:Number,
        OS:String,
        DeviceId:String,
        DeviceToken:String,
        AppVersion:String,
        LastOnline:String,
        DeviceModel:String,
        DeviceMake:String,
        Active:String
    }],
    location: {
        longitude:Number,
        latitude:Number
    },
    sessionToken:String,
    CurrentStatus:Number,
    Status:Number,
    LastOnline:String,
    app_ids:String,
    newapp_ids:String,
    Location: {
        Longitude:Number,
        Latitude:Number
    }
},{ collection: 'Drivers' });
Drivers.index({ Point: '2dsphere' });
module.exports = mongoose.model('Drivers', Drivers);