var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ZONES = new Schema({
    city:String,
    city_id:String,
    title:String,
    polygonProps:{
        paths:[{
            lat:String,
            lng:String
        }],
        strokeColor:String,
        strokeOpacity:Number,
        strokeWeight:Number,
        fillColor:String,
        fillOpacity:Number,
        draggable:Boolean,
        editable:Boolean,
        visible:Boolean
    },
    polygons:{
        type: [Number],
        index: '2d',
        coordinates:[
            [
                [

                ]
            ]
        ]
    },
    zoneseq:Number,
    pricing:[{
        id:String,
        instant:String,
        hrdelivery:String,
        samedaydelivery:String
    }]
},{ collection: 'zones' });
ZONES.index({ Point: '2dsphere' });
module.exports = mongoose.model('zones', ZONES);