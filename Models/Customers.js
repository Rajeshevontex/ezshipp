var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Customers = new Schema({

    customerseqId: Number,
    signupflag: Number,
    First_name:String,
    Email:String,
    Phone: String,
    countryCode:String,
    Verify: Number,
    Code: Number,
    stripeId:String,
    PasswordHash:String,
    PasswordSalt: String,
    CurrentStatus:Number,
    sessionToken:String,
    First_Time_Login:Boolean,
    terms_cond:Number,
    referral_code: String,
    location: {
        longitude:String,
        latitude:String
    },
    Location: {
        longitude:String,
        latitude:String
    },
    Devices:[{
        DeviceType:Number,
        OS:String,
        DeviceId:String,
        DeviceToken:String,
        AppVersion:String,
        LastOnline:String,
        DeviceModel:String,
        DeviceMake:String,
        Active:String
    }],
    AddressLog: {type:Array , default:[]},
    status:Number,
    CustomerImage:String,
    Status:Number,
    Created_dt:String
},{ collection: 'Customers' });
module.exports = mongoose.model('Customers', Customers);