var msg91mod = function(){

    this.crypto = require('crypto');
    this.uuid = require('node-uuid');
    this.rand = require('csprng');
    this.ApiResponce = require("../Models/Apiresponce.js");
    this.ApiMessages = require("../Models/Apimessages.js");
    this.Config = require("../Config/config.js");
    this.config = require("../Config/config.js");
    this.Counters = require('../Models/Counters.js');
    this.Customers = require('../Models/Customers.js');
    this.randomstring = require("randomstring");

    this.TWILIO_ACCOUNT_SID = this.config.twilio.ACCOUNT_SID;
    this.TWILIO_AUTH_TOKEN = this.config.twilio.AUTH_TOKEN;

    this.twilio = require('twilio');
    this.client = new this.twilio.RestClient(this.TWILIO_ACCOUNT_SID, this.TWILIO_AUTH_TOKEN);
    this.msg91 = require("msg91")(this.config.msg91.authkey, this.config.msg91.sender_id, this.config.msg91.route_no);   // "AUTHKEY", "SENDER_ID", "ROUTE_NO"


};
//Send OTP to Customer Phone Number
msg91mod.prototype.sendsms = function (Phone,otp,callback) {
    var me = this;
    var Message = 'One Time Password for ezshipp is ' + otp + ', Please use this password to login. ';

    me.msg91.send(Phone, Message, function (err, response) {
        return callback(false,'Message Sent Successfully');
    });
};

//Send Message to Customer Phone Number
msg91mod.prototype.sendsmstocustomer = function (Phone,Message,callback) {
    var me = this;

    me.msg91.send(Phone, Message, function (err, response) {
        return callback(false,'Message Sent Successfully');
    });
};
module.exports = msg91mod;