var customermod = function(){

    this.crypto = require('crypto');
    this.uuid = require('uuid');
    this.rand = require('csprng');
    this.ApiResponce = require("../Models/Apiresponce.js");
    this.ApiMessages = require("../Models/Apimessages.js");
    this.Config = require("../Config/config.js");
    this.config = require("../Config/config.js");
    this.Counters = require('../Models/Counters.js');
    this.Customers = require('../Models/Customers.js');
    this.CUSTOMERSESSION = require('../Models/CUSTOMERSESSION.js');
    this.CustomerPasswordTries = require('../Models/CustomerPasswordTries.js');
    this.Cookie = require('../Models/Cookie.js');
    this.COOKIE_TOKEN_SECURITY = require('../Models/COOKIE_TOKEN_SECURITY.js');
    this.CustomerForgotPasswordTries = require('../Models/CustomerForgotPasswordTries.js');
    this.Orders = require('../Models/Orders.js');
    this.ZONES = require('../Models/ZONES.js');
    this.OTP = require('../Models/OTP.js');
    this.twillomod = require('../CoreModules/twillomod.js'); // Setting the Path for Twillo Modules
    this.TwilloMod = new this.twillomod();
    this.moment = require('moment');
    this.msg91mod = require('../CoreModules/msg91mod.js'); // Setting the Path for Twillo Modules
    this.MSG91MOD = new this.msg91mod();
    this.ObjectID = require('mongodb').ObjectID;
};

//Check Whether CustomerID Exist or Not for Security
customermod.prototype.Check_for_CustomerID = function (values,callback) {
    var me = this;
    var error;
    var query = {
        _id:values.CustomerID
    };
    me.Customers.findOne(query).exec(function (err,Result) {
        if(Result){
            error = false;
            return callback(false,Result);
        }else {
            error = true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.CUSTOMER_NOT_FOUND}}));
        }
    })
};

//Update Customer Password
customermod.prototype.Update_Customer_Password = function (values,callback) {
  var me = this;
    var Password = values.Password;
    var salt = this.rand(160, 36);
    var pass = Password + salt ;
    var query = {
        _id:values.CustomerID
    };
    var changes = {
        PasswordHash: this.crypto.createHash('sha512').update(pass).digest("hex"),
        PasswordSalt: salt,
        First_Time_Login:false
    };
    var multiplicity = {
        multi:false
    };
    me.Customers.update(query,changes,multiplicity).exec(function (err,Result) {
        if(Result){
                return callback(false,new me.ApiResponce({ success: true, extras: { Status:'Password Updated Successfully' } }) );
        }else {
            console.log(JSON.stringify(err));
        }
    })
};

//Generate Random Password and Update IT in Customer Schema
customermod.prototype.GenerateRandomPasswordandUpdateItinSchema = function (values,CustomerID,First_name,callback) {
    var me = this;
    var Password = me.RandomNumber();
    var salt = this.rand(160, 36);
    var pass = Password + salt ;
    var query = {
        _id:CustomerID
    };
    var changes = {
        PasswordHash: this.crypto.createHash('sha512').update(pass).digest("hex"),
        PasswordSalt: salt,
        First_Time_Login:true
    };
    var multiplicity = {
        multi:false
    };
    me.Customers.update(query,changes,multiplicity).exec(function (err,Result) {
        if(Result){
            var Message = 'Dear '+First_name+' , Your New Password is '+Password+' . Regards Team Ezshipp';
            var PhoneNumber = values.countryCode + values.Phone;
            me.MSG91MOD.sendsmstocustomer(PhoneNumber,Message,function (err,Message) {
                return callback(false,new me.ApiResponce({ success: true, extras: { Status:'Password as been Reset Successfully' } }) );
            })
        }else {
            console.log(JSON.stringify(err));
        }
    })
};
//Finding Customer Forgot Password Tries
customermod.prototype.findCustomerForgotPasswordTries = function(values,callback) {
    var me = this;
    var error;
    var valid_attempts = new Date(me.moment().subtract(30, 'minute').toISOString());
    me.CustomerForgotPasswordTries.find({Phone:values.Phone,time:{$gt:valid_attempts}}, function(err,Result){
        var Count =Result.length;
        if(Count>=5){
            error=true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.FORGOT_PASSWORD_TRIES_LIMIT_EXCEDDED}}));
        }else {
            error=false;
            return callback(error);
        }

    });
};

//Storing Customer Forgot Password Tries
customermod.prototype.RegisterCustomerForgotPasswordTries = function (values,callback) {
    var me = this;
    var error;
    var date = new Date();
    var CustomerPasswordTryData = new me.CustomerForgotPasswordTries({
        Phone:values.Phone,
        time:date
    });
    CustomerPasswordTryData.save(function (err,Result) {
        error = false;
        return callback(error,'Forgot Password Try Registered Successfully');
    });
};


customermod.prototype.Find_USER_ORDER_DETAILS = function (values,callback) {
  var me = this;
    me.Orders.find({userId:values.CustomerID}).sort({order_datetime: -1}).exec(function(err,Result){
        var OrderData = [];
        var t = 0;
        for(var i =0; i<Result.length;i++){
            OrderData.push({
                pickAddress:Result[t].pickAddress,
                dropAddress:Result[t].dropAddress,
                paymentType:Result[t].paymentType,
                receiverName:Result[t].receiverName,
                receiverPhone:Result[t].receiverPhone,
                itemName:Result[t].itemName,
                itemDescription:Result[t].itemDescription,
                bookingType:Result[t].bookingType
            });
            t++;
        }
        return callback(false,new me.ApiResponce({ success: true, extras: { OrderData:OrderData} }) );
    })
};

//finding zone related to pickup and delivery locations
customermod.prototype.Zone_Finding_of_Order = function (values,OrderData,callback) {
    var me = this;
    var error;
    var zone = me.ZONES;
    var Order = me.Orders;

    var pickLatitude = parseFloat(values.pickLatitude);
    var pickLongitude = parseFloat(values.pickLongitude);
    var dropLatitude = parseFloat(values.dropLatitude);
    var dropLongitude = parseFloat(values.dropLongitude);
    zone.findOne({ 'polygons': { $geoIntersects: { $geometry: { type: "Point", coordinates: [pickLongitude, pickLatitude] } } } }, function (err, pickupzoneObj) {
        console.log("PICKUP Validate Error "+JSON.stringify(err));
        console.log("PICKUP Validate Suceess "+JSON.stringify(pickupzoneObj));
        if (pickupzoneObj) {
                zone.findOne({ 'polygons': { $geoIntersects: { $geometry: { type: "Point", coordinates: [dropLongitude, dropLatitude] } } } }, function (err, dropupzoneObj) {
                        if (dropupzoneObj) {

                            if (pickupzoneObj.city_id == dropupzoneObj.city_id) {

                                var updateObj = {
                                    $set: {
                                        "orderId": String(OrderData._id),
                                        "pickupdepo": pickupzoneObj.zoneseq,//String(pickupzoneObj._id),
                                        "pickupdeponame": pickupzoneObj.title,
                                        "deliverydepo": dropupzoneObj.zoneseq,//String(dropupzoneObj._id),
                                        "deliverydeponame": dropupzoneObj.title
                                    }
                                };

                                Order.update({ "_id": String(OrderData._id) }, updateObj, function (err, resultObj) {
                                    if (!err) {
                                        var reqdata = {
                                            "orderid": String(OrderData._id),
                                            "pickupdepo": String(pickupzoneObj._id),
                                            "plat": pickLatitude,
                                            "plng": pickLongitude
                                        };

                                        error = false;
                                        return callback(error,new me.ApiResponce({ success: true, extras: { Status:'Order Placed  Sucessfully'} }) );
                                        // fetchdrivers_orderanything(reqdata, function (err, obj) {
                                        //     if (!err) {
                                        //         error = false;
                                        //         return callback(error,obj);
                                        //     }
                                        // })
                                    }
                                })
                            } else {
                                error = true;
                                return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.PICKUP_ZONE_AND_DROP_ZONE_MUST_BE_IN_SAME_CITY}}));
                            }
                        } else {
                            // console.log("dropup location zone/depo not found");
                            error = true;
                            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.DROP_ZONE_NOT_IN_RANGE}}));
                        }
                })
            } else {
                //console.log("pickup location zone/depo not found");
                error = true;
                return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.PICKUP_ZONE_NOT_IN_RANGE}}));
            }

    });
    // function fetchdrivers_orderanything(reqdata, callback) {
    //     var driversarr = [];
    //     var Driver = db.collection("Drivers"); //"depoId": reqdata.pickupdepo,"status": 3,
    //     Driver.find({ $and: [{ "depoId": reqdata.pickupdepo, "acc_status": 3 }] }).toArray(function (err, driverslist) {
    //         if (!err) {
    //             if (driverslist.length > 0) {
    //                 var originsarr = [];
    //                 var listarr = [];
    //                 var list = [];
    //                 var finallist = [];
    //                 for (var i = 0; i < driverslist.length; i++) {
    //                     var latLong1 = driverslist[i].location.latitude + "," + driverslist[i].location.longitude
    //                     originsarr.push(latLong1.toString());
    //                     list.push(driverslist[i])
    //                 }
    //
    //                 var origins = originsarr;
    //                 var dlatlong = reqdata.plat + "," + reqdata.plng;
    //                 var destinations = [dlatlong.toString()]
    //                 distance.matrix(origins, destinations, function (err, distances) {
    //                     if (!err) {
    //                         if (distances.status == 'OK') {
    //                             for (var i = 0; i < origins.length; i++) {
    //                                 for (var j = 0; j < destinations.length; j++) {
    //                                     var origin = distances.origin_addresses[i];
    //                                     var destination = distances.destination_addresses[j];
    //                                     if (distances.rows[i].elements[j].status == 'OK' ) {
    //                                         var distance = distances.rows[i].elements[j].distance.text;
    //                                         var duration = distances.rows[i].elements[j].duration.value;
    //                                         var duration1 = distances.rows[i].elements[j].duration.text;
    //                                         var distancelocation = distances.rows[i].elements[j].distance.value;
    //                                         distancelocation = distancelocation / 1000;
    //                                         distancelocation = parseFloat(distancelocation).toFixed(2);
    //                                         listarr.push({
    //                                             'distance': distancelocation,
    //                                             'duration': duration,
    //                                             'flag': 1
    //                                         })
    //                                     } else {
    //                                         console.log(destination + ' is not reachable by land from ' + origin);
    //                                         listarr.push({
    //                                             'distance': "",
    //                                             'duration': "",
    //                                             'flag': 2
    //                                         })
    //                                     }
    //                                 }
    //                             }
    //
    //                             for (var i = 0; i < listarr.length; i++) {
    //                                 list[i].distance = listarr[i].distance;
    //                                 list[i].duration = listarr[i].duration;
    //                                 list[i].flag = listarr[i].flag;
    //                             }
    //
    //                             list.filter(function (el) {
    //                                 return el.flag == 1;
    //                             });
    //
    //                             function sortNumber(a, b) {
    //                                 return a.duration - b.duration;
    //                             }
    //                             list.sort(sortNumber);
    //                             if (list.length > 0) {
    //
    //                                 var Order = db.collection('Orders');
    //                                 var orderid = new ObjectID(reqdata.orderid);
    //                                 async.each(list, function (item, callback) {
    //                                     item.driverid = String(item._id);
    //                                     Order.findOne({ "_id": orderid }, function (err, orObj) {
    //                                         if (!err) {
    //                                             if (orObj) {
    //                                                 //before sending push checking driver free or not
    //                                                 var did = new ObjectID(item.driverid);
    //                                                 var driver = db.collection('Drivers');  //'status': 3
    //                                                 driver.findOne({ $and: [{ '_id': did, "acc_status": 3 }] }, function (err, driverdata) {
    //                                                     if (!err) {
    //                                                         if (driverdata) {
    //                                                             var dtime = moment().format('YYYY-MM-DD HH:mm:ss');
    //                                                             var timestamp = Date.now();
    //                                                             var updobj = { $push: { newapp_ids: { 'app_id': String(orderid), "timestamp": timestamp, "datetime": dtime } } };
    //                                                             var queryObj = { "_id": did };
    //                                                             driver.update(queryObj, updobj, function (err, Obj) {
    //                                                                 if (!err) {
    //                                                                     var publishConfig = {
    //                                                                         channel: item.driverid,
    //                                                                         message: {
    //                                                                             'orderid': String(orderid),
    //                                                                             'bid': orObj.orderseqId,
    //                                                                             'a': 21,                                         //21 new order request
    //                                                                             'ordermsg': 'new order waiting for driver'
    //                                                                         }
    //                                                                     }
    //                                                                     pubnub.publish(publishConfig, function (status, response) {
    //                                                                         console.log("publish to driver ");
    //                                                                     })
    //
    //                                                                     var mssageto = {
    //                                                                         "orderid": String(orderid),
    //                                                                         'bid': orObj.orderseqId,
    //                                                                         "a": 21,                                         //21 new order request
    //                                                                         "ordermsg": "new order waiting for driver"
    //                                                                     }
    //                                                                     //sending push to driver
    //                                                                     sendpushtodriver(item.driverid, mssageto);
    //
    //                                                                     Order.findOne({ $and: [{ "_id": orderid, driversLog: { $elemMatch: { "driverid": String(item.driverid), "status": "1" } } }] }, function (err, result3) {
    //                                                                         if (result3) {
    //                                                                             Order.update(
    //                                                                                 { $and: [{ "_id": orderid, driversLog: { $elemMatch: { "driverid": String(item.driverid), "status": "1" } } }] },
    //                                                                                 { $set: { "driversLog.$.driverid": String(item.driverid), "driversLog.$.status": "1", "driversLog.$.datetime": dtime, "driversLog.$.timestamp": timestamp } },
    //                                                                                 function (err, Obj) {
    //                                                                                     if (!err) {
    //                                                                                         callback();
    //                                                                                     }
    //                                                                                 })
    //                                                                         } else {
    //                                                                             Order.update(
    //                                                                                 { $and: [{ "_id": orderid }] },
    //                                                                                 {
    //                                                                                     $push: {
    //                                                                                         "driversLog": {
    //                                                                                             "driverid": String(item.driverid),
    //                                                                                             "status": "1",
    //                                                                                             "datetime": dtime,
    //                                                                                             "timestamp": timestamp
    //
    //                                                                                         }
    //                                                                                     }
    //                                                                                 },
    //                                                                                 function (err, Obj) {
    //                                                                                     callback();
    //                                                                                 })
    //                                                                         }
    //                                                                     })
    //                                                                 }
    //                                                             })
    //                                                         } else {
    //                                                             callback();
    //                                                         }
    //                                                     }
    //                                                 })
    //                                             }
    //                                         }
    //                                     })
    //                                 }, function (err) {
    //                                     var data = {
    //                                         "errNum": 1004,
    //                                         "errMsg": "we will dispatch the driver shortly to pickup your package",
    //                                         "errFlag": 0
    //                                     }
    //                                     callback(null, data);
    //                                 })
    //                             } else {
    //                                 var data = {
    //                                     "errNum": 1004,
    //                                     "errMsg": "we will dispatch the driver shortly to pickup your package",
    //                                     "errFlag": 0
    //                                 }
    //                                 callback(null, data);
    //                             }
    //                         }
    //                     }
    //                 })
    //             } else {
    //
    //                 var data = {
    //                     "errNum": 1004,
    //                     "errMsg": "we will dispatch the driver shortly to pickup your package",
    //                     "errFlag": 0
    //                 }
    //                 callback(null, data);
    //             }
    //         }
    //     })
    // }
};


//Storing the Customer Order Details
customermod.prototype.Storing_Customer_Order_Details = function (values,SequenceNumber,callback) {
    var me = this;
    var error;
    var pickAddress = values.pickAddress;
    var dropAddress = values.dropAddress;
    var pickLatitude = parseFloat(values.pickLatitude);
    var pickLongitude = parseFloat(values.pickLongitude);
    var dropLatitude = parseFloat(values.dropLatitude);
    var dropLongitude = parseFloat(values.dropLongitude);
    var moment = require('moment');
    var timestamp = moment(values.order_datetime);
    timestamp = moment(timestamp).valueOf();
    var OrderData = new me.Orders({
        "orderseqId": "E0000" + SequenceNumber,
        "userId": values.CustomerID,
        "order_datetime": values.order_datetime,
        "orderType": Number(values.orderType),
        "bookingType": Number(values.bookingType),
        "pickAddress": pickAddress,
        "receiverName": values.receiverName,
        "receiverPhone": values.receiverPhone,
        "paymentId": values.paymentId,
        "pickLocation": {
            "Longitude": pickLongitude,
            "Latitude": pickLatitude
        },
        "dropAddress": dropAddress,
        "dropLocation": {
            "Longitude": dropLongitude,
            "Latitude": dropLatitude
        },
        "itemName": values.itemName,
        "itemDescription": values.itemDescription,
        "itemImage": values.itemImage,
        "paymentType": values.paymentType,
        "deliverycharge": values.deliverycharge,
        "Devices": {
            "DeviceType": 3,
            "Os": "BROWSER"
        },
        "status": 1,
        "driversLog": [],
        "eventLog": [
            {
                "datetime": values.order_datetime,
                "status": '1',
                "timestamp": timestamp
            }
        ],

    });
    OrderData.save(function (err,OrderDetails) {
        console.log("Order Error "+JSON.stringify(err));
        console.log("Order Success "+JSON.stringify(OrderDetails));
        error = false;
        return callback(error,new me.ApiResponce({ success: true, extras: { Status:'Order Stored Sucessfully',OrderDetails:OrderDetails } }) );
    });


};

//Check Validity of Field for Orders Anything
customermod.prototype.Check_Validity_Orders_Anything = function(values,callback)
{
    var me = this;
    var error;
    var validator = require('validator');
    if(values.pickAddress==null||values.dropAddress==null||values.pickLatitude==null||values.pickLongitude==null||values.dropLatitude==null||values.dropLongitude==null||values.itemName==null||values.itemDescription==null||values.CustomerID==null||values.order_datetime==null||values.orderType==null||values.bookingType==null||values.receiverName==null||values.receiverPhone==null||values.paymentType==null||values.deliverycharge==null)
    {
        error = true;
        return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.ENTER_ALL_TAGS}}));
    }
    else
    {
        error = false;
        return callback(error);
    }
};

//Find and Update Order Sequence in Counters
customermod.prototype.Find_and_Update_Order_Sequence = function(callback){
  var me = this;
    me.Counters.findOneAndUpdate({ _id: "orderid" },
        { $set: { _id: "orderid" }, $inc: { "seq": 1 } },
        { upsert: true, returnNewDocument: true }).exec(function (err,Result) {
            var SequenceNumber = Result.seq;
        return callback(false,parseInt(SequenceNumber)+1);
    })
};




//Checking Whether Customer Session is Exist or Not Based on Customer ID
customermod.prototype.Check_Customer_Session = function (CustomerData,callback) {
    var me = this;
    var error;
    var query = {
        CustomerID:CustomerData._id
    };
    me.CUSTOMERSESSION.findOne(query).exec(function (err,Result) {
        if(Result){
            error = false;
            return callback(error);
        }else {
            error = true;
            return callback(error);
        }
    })
};

//Registering the Customer Session and Generating the Session ID
customermod.prototype.RegisteringCustomerSession = function (CustomerData,callback) {
var me = this;
    var SessionID = me.uuid();
    var CustomerID = CustomerData._id;
    var date = new Date();
    var error;
    var SessionData = new me.CUSTOMERSESSION({
        CustomerID:CustomerID,
        SessionID:SessionID,
        created_at:date,
        updated_at:date
    });
    SessionData.save();
    var CustomerData = {
        CustomerID:CustomerID,
        First_name:CustomerData.First_name,
        SessionID:SessionID,
        First_Time_Login:CustomerData.First_Time_Login
    };
    error = false;
    return callback(error,new me.ApiResponce({ success: true, extras: { Status:'Login Successfully',CustomerData:CustomerData } }) );
};

//Updating teh Customer Session and Updating the new Session ID
customermod.prototype.UpdatingCustomerSession = function (CustomerData,callback) {
    var me = this;
    var SessionID = me.uuid();
    var CustomerID = CustomerData._id;
    var date = new Date();
    var error;
    var CustomerData = {
        CustomerID:CustomerID,
        SessionID:SessionID,
        First_name:CustomerData.First_name,
        First_Time_Login:CustomerData.First_Time_Login
    };
    var query = {
        CustomerID:CustomerID,
    };
    var changes = {
        SessionID:SessionID,
        updated_at:date
    };
    var multiplicity = {
        multi:false
    };
    me.CUSTOMERSESSION.update(query,changes,multiplicity).exec(function (err,Result) {
       if(Result){
           error = false;
           return callback(error,new me.ApiResponce({ success: true, extras: { Status:'Login Successfully',CustomerData:CustomerData } }) );
       }
    });

};

//Finding Customer Password Tries
customermod.prototype.findCustomerPasswordTries = function(values,callback) {
    var me = this;
    var error;
    var valid_attempts = new Date(me.moment().subtract(30, 'minute').toISOString());
    me.CustomerPasswordTries.find({Phone:values.Phone,time:{$gt:valid_attempts}}, function(err,Result){
        var Count =Result.length;
        if(Count>=5){
            error=true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.PASSWORD_TRIES_LIMIT_EXCEDDED}}));
        }else {
            error=false;
            return callback(error);
        }

    });
};

//Storing Customer Password Tries
customermod.prototype.RegisterCustomerPasswordTries = function (values,callback) {
var me = this;
    var error;
    var date = new Date();
    var CustomerPasswordTryData = new me.CustomerPasswordTries({
        Phone:values.Phone,
        time:date
    });
    CustomerPasswordTryData.save(function (err,Result) {
        error = false;
        return callback(error,'Password Try Registered Successfully');
    });
};
//Removing Customer Password Tries
customermod.prototype.DeleteCustomerPasswordTries = function(values,callback) {
    var me = this;
    me.CustomerPasswordTries.remove({Phone:values.Phone}, function(err,Result){
        if(Result){
            var error=false;
            return callback(error, new me.ApiResponce({success:true}));
        }
    });
};
//Customer Singin Module
customermod.prototype.CustomerSignIn = function (values,CustomerData,callback) {
  var me = this;
    var error;
    var Salt = CustomerData.PasswordSalt;
    var newpass = values.Password + Salt;
    var ExistPasswordHash = CustomerData.PasswordHash;
    var NewPasswordHash = this.crypto.createHash('sha512').update(newpass).digest("hex");
    if(ExistPasswordHash === NewPasswordHash){
        me.DeleteCustomerPasswordTries(values,function (err,Result) {
            error = false;
            return callback(error,'Login Successful')
        });
    }else {
        me.RegisterCustomerPasswordTries(values,function (err,Result) {
            error = true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.INVALID_PASSWORD}}));
        });
    }
};
//Check Whether Phone Number Exist or Not for Loginn and Security
customermod.prototype.Check_Whether_PhoneNumber_Exist = function (values,callback) {
    var me = this;
    var error;
    var query = {
        Phone:values.Phone
    };
    me.Customers.findOne(query).exec(function (err,Result) {
        if(Result){
            error = false;
            return callback(false,Result);
        }else {
            error = true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.PHONE_NUMBER_NOT_EXIST}}));
        }
    })
};
//Check Validity of Field for Login
customermod.prototype.Check_Validity_Signin = function(values,callback)
{
    var me = this;
    var error;
    var validator = require('validator');
    if(values.Phone==null||values.Password==null)
    {
        error = true;
        return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.ENTER_ALL_TAGS}}));
    }
    else
    {
            error = false;
            return callback(error);
    }
};



//Saving the Signup Data in Schema
customermod.prototype.Customer_Signup = function (values,CustomerLength,stripeId,callback) {
  var me = this;
    var salt = this.rand(160, 36);
    var Password = values.Password;
    var pass = Password + salt ;
    var date=new Date();
    var SignupData = new me.Customers({
        Status:1,
        customerseqId:CustomerLength,
        signupflag:1,
        First_name:values.First_name,
        Email:values.Email,
        Phone:values.Phone,
        countryCode:values.countryCode,
        Verify:0,
        Code:values.Code,
        stripeId:stripeId,
        PasswordHash: this.crypto.createHash('sha512').update(pass).digest("hex"),
        PasswordSalt: salt,
        First_Time_Login:false,
        CurrentStatus:1,
        terms_cond:1,
        referral_code:''
    });
    var Message = 'Dear '+values.First_name+' , You have Successfully Registered for Ezshipp Services.';
    var PhoneNumber = values.countryCode + values.Phone;
    me.MSG91MOD.sendsmstocustomer(PhoneNumber,Message,function (err,Message) {
           SignupData.save(function (err,Result) {
               return callback(false,new me.ApiResponce({ success: true, extras: { Status:'Signup Successfully' } }) );
           })
    });
};


//Checking the Validity of Feilds for Validating OTP
customermod.prototype.Check_Validity_Sign_Up = function(values,callback)
{
    var me = this;
    var error;
    var validator = require('validator');
    if(values.First_name==null||values.Email==null||values.Phone==null||values.countryCode==null||values.Code==null||values.Password==null||values.terms_cond==null)
    {
        error = true;
        return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.ENTER_ALL_TAGS}}));
    }
    else
    {
        var terms_cond = parseInt(values.terms_cond);
        if(terms_cond == 1){
            error = false;
            return callback(error);
        }else {
            error = true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.TERM_AND_CONDITIONS_NOT_ACCEPTED}}));
        }
    }
};


//Updating the Sequence Number of Customer
customermod.prototype.UpdatetheCustomerSeqID = function (CustomerLength,callback) {
    var me = this;
    var query = {
        "_id": "customerid"
    };
    var changes=  {
        seq:CustomerLength
    };
    var multiplicity = {
        multi:false
    };
    me.Counters.update(query,changes,multiplicity).exec(function (err,result) {
        if(result){
            return callback(false,'Sequence Number Updated Successfully');
        }
    })
};

//Validating the Phone Number with Latest OTP
customermod.prototype.ValidateLatestOTP = function (values,callback) {
  var me = this;
    var error;
    var query = {
        Phone:values.Phone,
        countryCode:values.countryCode,
        OTP:values.OTP,
        Latest:true
    };
    me.OTP.findOne(query).exec(function (err,Result) {
        if(Result){
            error = false;
            return callback(error,new me.ApiResponce({ success: true, extras: { Status:'OTP Validated Successfully',OTP:values.OTP } }) );
        }else {
            error = true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.INVALID_OTP}}));
        }
    })
};
//REMOVE THE OTP BY PHONE WHEN VALIDATION IS SUCCESSFULLY DONE ONLY TO LATEST FALSE
customermod.prototype.RemoveLatestFalseOTPofPhone = function (values,callback) {
  var me = this;
    var error;
    var query = {
        Phone:values.Phone,
        countryCode:values.countryCode,
        Latest:false
    };
    me.OTP.find(query).remove().exec(function (err,result) {
        return callback(false,"Unwanted Phone OTP's Removed");
    })
};

//Getting the Sequence Number of Customer
customermod.prototype.FindtheCustomerSeqID = function (callback) {
  var me = this;
    me.Counters.findOne({"_id": "customerid"}).exec(function (err,result) {
        if(result){
            return callback(false,result.seq);
        }
    })
};
//Check for Number of Phone Number Registered to Cookie if more than 5 than pop up Error
customermod.prototype.FindCookiePhoneRegistered = function (values , callback) {
    var me = this;
    var error ;
    me.COOKIE_TOKEN_SECURITY.find({Cookie:values.Cookie}).exec(function (err,Result) {
        var Length = parseInt(Result.length);
        if(Length >= 5){
            error = true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.DAILY_QUOTA_FOR_SIGNUP_REACHED_IN_DEVICE}}));
        }else {
            error = false;
            return callback(false);
        }
    })
};

//Check Whether Phone Already Registered or Not with Country Code
customermod.prototype.Check_Whether_Phone_Registered = function (values,callback) {
    var me = this;
    var error;
    var query = {
        Phone:values.Phone,
        countryCode:values.countryCode
    };
    me.Customers.findOne(query).exec(function (err,Result) {
        if(Result){
            error = true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.PHONE_NUMBER_ALREADY_REGISTERED}}));
        }else {
            error = false;
            return callback(false);
        }
    })
};
//Check Whether Email Already Registered or Not
customermod.prototype.Check_Whether_Email_Registered = function (values,callback) {
    var me = this;
    var error;
    var query = {
        Email:values.Email
    };
    me.Customers.findOne(query).exec(function (err,Result) {
        if(Result){
            error = true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.EMAIL_ALREADY_REGISTERED}}));
        }else {
            error = false;
            return callback(false);
        }
    })
};
//Getting the Count of OTP for a Phone Number and if OTP Request More than 5 than Pop up Error
customermod.prototype.CheckforPhoneNumberCount = function (values,callback) {
  var me = this;
    var error ;
    var valid_attempts = new Date(me.moment().subtract(1200, 'minute'));

    var query = {
        Phone:values.Phone,
        countryCode:values.countryCode,
        time:{
            $gt:valid_attempts
        }
    };
    me.OTP.find(query).exec(function (err,result) {
        var Count = parseInt(result.length);
        if(Count >=5){
            error = true;
            return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.OTP_REQUEST_EXCEEDED_FOR_DAY}}));
        }else {
            error = false;
            return callback(false);
        }
    })
};

//Storing the Cookie for Phone if Phone Number Doesnt Register to Cookie if Registered than Dont Save
customermod.prototype.RegisterCookiePhoneToken = function (values,callback) {
  var me = this;
    me.COOKIE_TOKEN_SECURITY.findOne({Cookie:values.Cookie,Phone:values.Phone}).exec(function (err,Result) {
       if(Result){
           return callback(false,'Phone Already Registered for Cookie');
       } else {
           var time = new Date();
           var CookiePhoneData = me.COOKIE_TOKEN_SECURITY({
               Phone:values.Phone,
               countryCode:values.countryCode,
               Cookie:values.Cookie,
               time:time,
               created_at:time,
               updated_at:time
           });
           CookiePhoneData.save();
           return callback(false,'Phone Number Registered to Cookie');
       }
    });
};


//Generating the OTP to Phone and Storing Everything
customermod.prototype.GenerateOTPandStoreITinSchema = function (values,callback) {
  var me = this;
    var randomnumber = me.RandomNumber();
    var time = new Date();
    var PhoneNumber =values.countryCode+values.Phone;
    var OTPData = new me.OTP({
        Phone:values.Phone,
        countryCode:values.countryCode,
        OTP:randomnumber,
        time:time,
        Latest:true,
        created_at:time,
        updated_at:time
    });
    OTPData.save(function (err,result) {
        if(result){
            me.MSG91MOD.sendsms(PhoneNumber,randomnumber,function (err,Message) {
               return callback(false,new me.ApiResponce({ success: true, extras: { Status:'OTP Send Successfully' } }) );
            });
        }
    })
};

//Update the Remaining Phone OTP's to false
customermod.prototype.UpdatePhoneOTPFalse = function (values,callback) {
  var me = this;
    var query = {
        Phone:values.Phone,
        countryCode:values.countryCode
    };
    var changes = {
        Latest:false
    };
    var multiplicity = {
      multi:true
    };
    me.OTP.update(query,changes,multiplicity).exec(function (err,responcer) {
        return callback(false,new me.ApiResponce({ success: true, extras: { Status:'Phone OTP Latest updated to false' } }) );
    })
};

//Generating the Random Number for Security
customermod.prototype.RandomNumber = function() {
    var me = this;
    var charBank = "1234567890";
    var fstring = '';
    for (var i = 0; i < 6; i++) {
        fstring += charBank[parseInt(Math.random() * 10)];
    }
    return parseInt(fstring);
};

//Checking the Validity of Feilds for Validating OTP
customermod.prototype.Check_Validity_Validating_OTP = function(values,callback)
{
    var me = this;
    var error;
    var validator = require('validator');
    if(values.OTP==null||values.Phone==null||values.countryCode==null)
    {
        error = true;
        return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.ENTER_ALL_TAGS}}));
    }
    else
    {
        error = false;
        return callback(error);
    }
};

//Checking the Validity of Feilds for Generating OTP
customermod.prototype.Check_Validity_Generating_OTP = function(values,callback)
{
    var me = this;
    var error;
    var validator = require('validator');
    if(values.Cookie==null||values.Phone==null||values.countryCode==null)
    {
        error = true;
        return callback(error, new me.ApiResponce({success:false,extras: {msg: me.ApiMessages.ENTER_ALL_TAGS}}));
    }
    else
    {
        error = false;
        return callback(error);
    }
};

//Generating the Session Cookie and Storing the Cookie
customermod.prototype.GeneratingtheSessionCookie = function (callback) {
  var me = this;
    var cookie = me.RandomStringCookie()+me.RandomNumber();
    var time = new Date();
    var CookieData = new me.Cookie({
        Cookie:cookie,
        time:time
    });
    CookieData.save(function (err,result) {
        if(result){
            return callback(false,new me.ApiResponce({ success: true, extras: { Status:'Cookie Created Successfully',Cookie:cookie } }) );
        }
    })
};

//Generating the Random Cookie for Security
customermod.prototype.RandomStringCookie = function() {
    var me = this;
    var charBank = "ezshippEZSHIPP1234567890";
    var fstring = '';
    for (var i = 0; i < 10; i++) {
        fstring += charBank[parseInt(Math.random() * 24)];
    }
    return fstring;
};
module.exports = customermod;