var mailgunmod = function(){

    this.crypto = require('crypto');
    this.uuid = require('node-uuid');
    this.rand = require('csprng');
    this.ApiResponce = require("../Models/Apiresponce.js");
    this.ApiMessages = require("../Models/Apimessages.js");
    this.Config = require("../Config/config.js");
    this.config = require("../Config/config.js");
    this.Counters = require('../Models/Counters.js');
    this.Customers = require('../Models/Customers.js');
    this.randomstring = require("randomstring");
    var config = this.config;
    this.mailgun = require('mailgun-js')({ apiKey: config.mailgun.api_key, domain: config.mailgun.domain });

};
//Send Registration Success to Customer after Signup
mailgunmod.prototype.sendEmailCustomerRegistration = function (Email,First_name,callback) {
    var me = this;
    var data = {
        from: me.config.mailgun.frommail,
        to: Email,
        subject: "Thank you for joining ezshipp",
        html: 'Dear ' +First_name+ ', <br><br> Thank you for signing up with ezshipp , we hope you find the best of stores on our app and save time and money! <br><br> Cheers, <br>  Team ezshipp'
    };
    me.mailgun.messages().send(data, function (error, body) {
        return callback(false,body);
    });
};
module.exports = mailgunmod;